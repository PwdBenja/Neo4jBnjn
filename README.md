# Guia_Neo4J

# Neo4J

Neo4j es un software libre de Base base de datos orientada a grafos.

## Instalación
Primer paso

```bash
sudo apt update
```

El segundo paso se realiza aquí para agregar un paquete para conexiones HTTPS, y si este paquete usa una biblioteca llamada libapt-pkg para habilitar HTTPS. Sin embargo, si el paquete ya está disponible, el siguiente comando actualizará ese paquete, por lo que es beneficioso en ambos aspectos. apt-transporte-https

```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```

Después de esto, debe agregar la clave GPG para verificar que la instalación se realiza de manera segura. Entonces, para agregar una clave GPG o Neo4j, copie y pegue el siguiente comando en la terminal para hacerlo:

```bash
sudo curl -fsSL https: // debian.neo4j.com / neotechnology.gpg.key | sudo apt-key agregar -
```

Agregue el repositorio de Neo4j a la lista de administradores de paquetes usando el comando mencionado a continuación:

```bash
sudo add-apt-repository “deb https: // debian.neo4j.com estable 4.1 ”
```

 finalmente, puede instalar Neo4j en su sistema; se observa que si no ha instalado paquetes de Java antes de esta instalación, el siguiente comando le pedirá que presione y para instalar todas las dependencias, incluidos los paquetes de Java (que son necesarios para esta instalación). Sin embargo, si ya instaló los paquetes de Java, no recibirá ningún aviso durante la instalación, y su instalación se completará de una sola vez: entonces, ejecute el siguiente comando en la terminal para instalar Neo4j:

```bash
sudo apt install neo4j
```
## Cómo verificar y habilitar el servicio Neo4J

```bash
sudo service neo4j start | restart | stop | status
```

## cyper-shell

```bash
cypher-shell
```
pwd: neo4j
user: neo4j
