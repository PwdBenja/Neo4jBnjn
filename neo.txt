create (benjamin:person {nombre: 'benjamin', edad: '29'})
create (juan:person {nombre: 'juan', edad: '19'})
create (felipe:person {nombre: 'felipe', edad: '31'})
create (esteban:person {nombre: 'esteban', edad: '17'})
create (javiera:person {nombre: 'javiera', edad: '29'})
create (juana:person {nombre: 'juana', edad: '19'})
create (fernanda:person {nombre: 'fernanda', edad: '31'})
create (luna:person {nombre: 'luna', edad: '17'})
create (estrella:person {nombre: 'estrella', edad: '29'})
create (maria:person {nombre: 'maria', edad: '19'})
create (beatriz:person {nombre: 'beatriz', edad: '31'})
create (zoila:person {nombre: 'zoila', edad: '17'})


create (increibles:movie {nombre: 'Los increibles', duracion: '120', tipo: 'fantasia' })
create (DoctorStrange:movie {nombre: 'Doctor Strange', duracion: '120', tipo: 'fantasia' })
create (Sonic2:movie {nombre: 'Sonic 2', duracion: '140', tipo: 'animacion' })
create (Lamomia:movie {nombre: 'La momia', duracion: '130', tipo: 'aventura' })
create (animalesf:movie {nombre: 'Animales Fantasticos', duracion: '120', tipo: 'fantasia' })
create (tiposmalos:movie {nombre: 'Los tipos malos', duracion: '120', tipo: 'accion' })
create (ciudadperdida:movie {nombre: 'La ciudad perdida', duracion: '140', tipo: 'comedia' })
create (desesperada:movie {nombre: 'Desesperada', duracion: '130', tipo: 'Suspenso' })
create (northman:movie {nombre: 'The NOrthMan', duracion: '120', tipo: 'Accion' })
create (hueso:movie {nombre: 'Hasta el hueso', duracion: '120', tipo: 'fantasia' })


create (golf:sport {nombre: 'Golf', categoria: 'individual'})
create (basquet:sport {nombre: 'Basquetbol', categoria: 'grupal'})
create (futbol:sport {nombre: 'Futbol', categoria: 'grupal'})
create (tenis:sport {nombre: 'tenis', categoria: 'grupal'})
create (ciclismo:sport {nombre: 'ciclismo', categoria: 'individual'})
create (natacion:sport {nombre: 'natacion', categoria: 'individual'})
create (voley:sport {nombre: 'voley', categoria: 'grupal'})
create (padel:sport {nombre: 'padel', categoria: 'duplas'})
create (badminton:sport {nombre: 'badminton', categoria: 'grupal'})
create (squash:sport {nombre: 'squash', categoria: 'individual'})


create (valdivia:city {nombre: 'Valdivia', Fase: '2'})
create (parral:city {nombre: 'Parral', Fase: '1'})
create (temuco:city {nombre: 'Temuco', Fase: '3'})
create (talca:city {nombre: 'Talca', Fase: '2'})
create (iquique:city {nombre: 'Iquique', Fase: '2'})
create (puertomontt:city {nombre: 'Puerto MOntt', Fase: '1'})
create (puntaarenas:city {nombre: 'Punta Arenas', Fase: '3'})
create (arica:city {nombre: 'Arica', Fase: '2'})
create (serena:city {nombre: 'La serena', Fase: '2'})
create (conce:city {nombre: 'Concepcion', Fase: '1'})
create (antofa:city {nombre: 'Antofagasta', Fase: '3'})
create (vina:city {nombre: 'Viña  del mar', Fase: '2'})
create (puertovaras:city {nombre: 'Puerto Varas', Fase: '2'})
create (copiapo:city {nombre: 'Copiapo', Fase: '1'})
create (calama:city {nombre: 'calama', Fase: '3'})


create (benjamin)-[:VIVE]->(talca)
create (juan)-[:VIVE]->(parral)
create (felipe)-[:VIVE]->(valdivia)
create (esteban)-[:VIVE]->(calama)
create (javiera)-[:VIVE]->(talca)
create (juana)-[:VIVE]->(temuco)
create (fernanda)-[:VIVE]->(antofa)
create (luna)-[:VIVE]->(talca)
create (estrella)-[:VIVE]->(parral)
create (maria)-[:VIVE]->(valdivia)
create (beatriz)-[:VIVE]->(parral)
create (zoila)-[:VIVE]->(iquique)
create (benjamin)-[:PRACTICA]->(futbol)
create (benjamin)-[:PRACTICA]->(golf)
create (juan)-[:PRACTICA]->(futbol)
create (juan)-[:PRACTICA]->(basquet)
create (felipe)-[:PRACTICA]->(tenis)
create (felipe)-[:PRACTICA]->(golf)
create (javiera)-[:PRACTICA]->(basquet)
create (juana)-[:PRACTICA]->(basquet)
create (fernanda)-[:PRACTICA]->(basquet)
create (luna)-[:PRACTICA]->(basquet)
create (estrella)-[:PRACTICA]->(basquet)
create (maria)-[:PRACTICA]->(basquet)
create (beatriz)-[:PRACTICA]->(basquet)
create (zoila)-[:PRACTICA]->(basquet)
create (felipe)-[:PRACTICA]->(basquet)
create (esteban)-[:PRACTICA]->(tenis)
create (javiera)-[:PRACTICA]->(basquet)
create (juana)-[:PRACTICA]->(basquet)
create (fernanda)-[:PRACTICA]->(basquet)
create (luna)-[:PRACTICA]->(basquet)
create (estrella)-[:PRACTICA]->(basquet)
create (maria)-[:PRACTICA]->(tenis)
create (beatriz)-[:PRACTICA]->(futbol)
create (zoila)-[:PRACTICA]->(golf)
create (benjamin)-[:OBSERVO]->(increibles)
create (juan)-[:OBSERVO]->(DoctorStrange)
create (felipe)-[:OBSERVO]->(Sonic2)
create (esteban)-[:OBSERVO]->(Lamomia)
create (javiera)-[:OBSERVO]->(animalesf)
create (juana)-[:OBSERVO]->(hueso)
create (fernanda)-[:OBSERVO]->(desesperada)
create (luna)-[:OBSERVO]->(northman)
create (estrella)-[:OBSERVO]->(ciudadperdida)
create (maria)-[:OBSERVO]->(tiposmalos)
create (beatriz)-[:OBSERVO]->(animalesf)
create (zoila)-[:OBSERVO]->(hueso)
create (felipe)-[:OBSERVO]->(Lamomia)
create (esteban)-[:OBSERVO]->(Sonic2)
create (javiera)-[:OBSERVO]->(hueso)
create (juana)-[:OBSERVO]->(desesperada)
create (fernanda)-[:OBSERVO]->(northman)
create (luna)-[:OBSERVO]->(ciudadperdida)
create (estrella)-[:OBSERVO]->(northman)
create (maria)-[:OBSERVO]->(northman)
create (beatriz)-[:OBSERVO]->(northman)
create (zoila)-[:OBSERVO]->(northman)


MATCH (benjamin:person { nombre: 'benjamin'}) 
set benjamin.cumpleanos = date("2021") 
return benjamin

MATCH (juan:person {nombre: 'juan', edad: '19'}) 
set juan.cumpleanos = date("2021") 
return juan

MATCH (felipe:person {nombre: 'felipe', edad: '31'}) 
set felipe.cumpleanos = date("2021") 
return felipe

MATCH (esteban:person {nombre: 'esteban', edad: '17'})
set esteban.cumpleanos = date("2021") 
return esteban

MATCH (javiera:person {nombre: 'javiera', edad: '29'}) 
set javiera.cumpleanos = date("2022") 
return javiera


MATCH (ciclismo:sport {nombre: 'ciclismo'})
detach delete ciclismo

MATCH (natacion:sport {nombre: 'natacion'})
detach delete natacion

MATCH (voley:sport {nombre: 'voley'})
detach delete voley

MATCH (padel:sport {nombre: 'padel'})
detach delete padel

MATCH (squash:sport {nombre: 'squash'})
detach delete squash




match (benjamin)-[:VIVE]->(talca)
delete r

create (juan)-[:VIVE]->(parral)
create (felipe)-[:VIVE]->(valdivia)
create (esteban)-[:VIVE]->(calama)
create (javiera)-[:VIVE]->(talca)




